type Task = {
    id?: string;
    date: string;
    text: string;
    status: Status;
  };