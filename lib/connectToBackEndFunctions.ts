import { convertDateForDB } from '@/lib/DateConvertingFunctions';
import { Status } from "@prisma/client";

export const sendTask = async (addedTask: Task) => {
  const response = await fetch("/api/tasks", {
    method: "POST",
    body: JSON.stringify(addedTask),
  });
  if (!response.ok) {
    throw new Error("Failed to send task");
  }

  return response;
};

export const getTasks = async (date: Date) => {
  const url = `/api/tasks?date=${convertDateForDB(date)}`;
  const response = await fetch(url);
  if (!response.ok) {
    throw new Error("Failed to fetch tasks");
  }

  return response.json();
};

export const deleteTask = async (id: string) => {
  const url = `/api/tasks?id=${id}`;
  const response = await fetch(url, {
    method: "DELETE",
  });

  if (!response.ok) {
    throw new Error("Failed to delete task");
  }
  return response;
};

export const updateTask = async (task: Task) => {
  const response = await fetch("/api/tasks", {
    method: "PATCH",
    body: JSON.stringify(task),
  });

  if (!response.ok) {
    throw new Error("Failed to update task");
  }

  return response.json();
};

export const convertStringToStatus = (status: string): Status => {
  const statusMap: { [key: string]: Status } = {
    new: Status.new,
    in_progress: Status.in_progress,
    complited: Status.complited,
    stuck: Status.stuck,
  };
  return statusMap[status] || Status.new;
};
