export const convertDateForDB = (date: Date | string): string => {
  const newDate = new Date(date);
  newDate.setHours(1, 0, 0, 0); //because of the time zone converting
  return newDate.toISOString();
};

export const convertDateForUI = (date: Date | string): string => {
  const newDate = new Date(date);
  const day = newDate.getDate();
  const monthName = newDate.toLocaleString("en-US", { month: "long" });
  return `${day} ${monthName}`;
};

export const convertDateToISOString = (date: Date | string): string => {
  const newDate = new Date(date);
  newDate.setHours(0, 0, 0, 0);
  return newDate.toISOString();
};
