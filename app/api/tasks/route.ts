import prisma from "@/lib/prisma";
import { Status } from "@prisma/client";

type PostRequestBody = {
  id: string;
  date: string;
  text: string;
  status: Status;
};

export const GET = async (request: Request) => {
  const url = request.url;
  const urlObject = new URL(url as string);
  const params = new URLSearchParams(urlObject.search);

  const userDate = params.get("date");

  if (!userDate) {
    return new Response("There is no data!", {
      status: 500,
    });
  }

  const task = await prisma.task.findMany({
    where: {
      date: userDate,
    },
  });

  return new Response(JSON.stringify(task), {
    status: 200,
  });
};

export const POST = async (request: Request) => {
  const body: PostRequestBody = await request.json();

  await prisma.task.create({
    data: {
      text: body.text,
      date: body.date,
      status: body.status,
      id: Math.random().toString(),
    },
  });

  return new Response(JSON.stringify("OK"), {
    status: 200,
  });
};

export const DELETE = async (request: Request) => {
  const url = request.url;
  const urlObject = new URL(url as string);
  const params = new URLSearchParams(urlObject.search);

  const id = params.get("id");

  await prisma.task.delete({
    where: {
      id: id as string,
    },
  });

  return new Response(JSON.stringify("OK"), {
    status: 200,
  });
};

export const PATCH = async (request: Request) => {
  const body: PostRequestBody = await request.json();
  const task = await prisma.task.findUnique({
    where: {
      id: body.id,
    },
  });

  if (!task) {
    return new Response("Task is not found", {
      status: 500,
    });
  }

  const updatedTask = await prisma.task.update({
    where: {
      id: task.id,
    },
    data: {
      text: body.text,
      status: body.status,
      date: body.date,
    },
  });

  return new Response(JSON.stringify(updatedTask), {
    status: 200,
  });
};
