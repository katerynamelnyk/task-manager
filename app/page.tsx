"use client";

import styles from "./page.module.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { useState } from "react";
import Calendar from "react-calendar";
import { Button, Spinner } from "react-bootstrap";
import "react-calendar/dist/Calendar.css";
import AddTask from "@/components/AddTask";
import AllTasks from "@/components/AllTasks";
import {
  sendTask,
  convertStringToStatus,
} from "@/lib/connectToBackEndFunctions";
import {
  convertDateForUI,
  convertDateForDB,
} from "@/lib/DateConvertingFunctions";

type Value = Date;

export default function Home() {
  const [value, onChange] = useState<Value>(new Date());
  const [isOpenAddTask, setIsOpenAddTask] = useState(false);

  const handleToggleAddTask = () => {
    setIsAllTasks(false);
    setIsOpenAddTask((prev) => !prev);
  };

  const [isLoading, setIsLoading] = useState(false);
  const [isAllTasks, setIsAllTasks] = useState(false);

  const handleShowAllTasks = () => {
    setIsOpenAddTask(false);
    setIsAllTasks((prev) => !prev);
  };

  const addTask = (text: string, status: string) => {
    if (!value) {
      return;
    }
    const mappedStatus = convertStringToStatus(status);
    const convertedDate = convertDateForDB(value);
    const task: Task = {
      date: convertedDate,
      text,
      status: mappedStatus,
    };
    setIsLoading(true);
    sendTask(task).then(() => setIsLoading(false));
    setIsOpenAddTask(false);
  };

  return (
    <main className={styles.main}>
      <div>
        <Calendar locale="en-EN" onChange={onChange} value={value} />
        <div className={styles.container}>
        <Button variant="outline-primary" onClick={handleToggleAddTask}>
          + New Task
        </Button>
        <Button variant="outline-primary" onClick={handleShowAllTasks}>
          All Tasks For {convertDateForUI(value)}
        </Button>
        </div>
        {isOpenAddTask && <AddTask onClick={addTask} />}
        {isAllTasks && <AllTasks date={value} />}
        {isLoading && <Spinner animation="border" />}
      </div>
    </main>
  );
}
