import React, { useState } from "react";
import { Button } from "react-bootstrap";
import AddTask from "./AddTask";
import {
  updateTask,
  convertStringToStatus,
} from "@/lib/connectToBackEndFunctions";
import styles from "./TaskItem.module.css";

type Props = {
  task: Task;
  onDelete: (id?: string) => void;
  onEdit: (id?: string) => void;
};

const TaskItem = ({ task, onDelete, onEdit }: Props) => {
  const [isEdit, setIsEdit] = useState(false);

  const onEditTask = () => {
    setIsEdit((prev) => !prev);
  };

  const onUpdateTask = (text: string, status: string) => {
    const convertedStatus = convertStringToStatus(status);
    const updatedTask: Task = {
      id: task.id,
      date: task.date,
      text,
      status: convertedStatus,
    };
    updateTask(updatedTask)
      .then(() => onEdit())
      .then(() => setIsEdit(false));
  };

  return (
    <>
      <div className={styles.container}>
        <p className={styles.content}>{task.text}</p>
        <p className={styles.content}>{task.status}</p>
        <Button variant="light" onClick={onEditTask}>
          Edit
        </Button>
        <Button variant="dark" onClick={() => onDelete(task.id)}>
          Delete
        </Button>
      </div>
      {isEdit && (
        <AddTask
          onClick={onUpdateTask}
          isEdit
          editedTaskStatus={task.status}
          editedTaskText={task.text}
        />
      )}
    </>
  );
};

export default TaskItem;
