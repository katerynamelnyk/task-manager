import { useEffect, useState } from "react";
import TaskItem from "./TaskItem";
import { getTasks, deleteTask } from "@/lib/connectToBackEndFunctions";
import { ListGroup } from "react-bootstrap";
import styles from './AllTasks.module.css'

type Props = {
  date: Date;
};

const AllTasks = ({ date }: Props) => {
  const [tasks, setTasks] = useState<Task[]>([]);
  const [isDeletedTask, setIsDeletedTask] = useState(false);
  const [isEditedTask, setIsEditedTask] = useState(false);

  useEffect(() => {
    getTasks(date).then(setTasks);
  }, [isDeletedTask, isEditedTask, date]);

  const onDeleteTask = (id?: string) => {
    if (!id) {
      return;
    }
    deleteTask(id);
    setIsDeletedTask(true);
  };

  const onEditTask = () => {
    setIsEditedTask(true);
  };

  const renderedTasks =
    tasks.length > 0 &&
    tasks.map((item) => (
      <ListGroup.Item key={item.id}>
        <TaskItem task={item} onDelete={onDeleteTask} onEdit={onEditTask} />
      </ListGroup.Item>
    ));

  return (
    <div className={styles.container}>
      <ListGroup variant="flush">{renderedTasks}</ListGroup>
      {tasks.length === 0 && <div>You have no tasks for this day</div>}
    </div>
  );
};

export default AllTasks;
