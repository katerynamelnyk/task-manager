import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import { ChangeEvent, useState } from "react";
import styles from './AddTask.module.css'

type Props = {
  onClick: (text: string, status: string) => void;
  isEdit?: boolean;
  editedTaskText?: string;
  editedTaskStatus?: string;
};

const AddTask = ({
  onClick,
  isEdit,
  editedTaskText,
  editedTaskStatus,
}: Props) => {
  const [taskText, setTaskText] = useState(isEdit ? editedTaskText : "");
  const [taskStatus, setTaskStatus] = useState(isEdit ? editedTaskStatus : "new");

  const onChangeInput = (e: any) => {
    setTaskText(e.target.value);
  };

  const onSelectStatus = (e: ChangeEvent<HTMLSelectElement>) => {
    setTaskStatus(e.currentTarget.value);
  };

  const onClickHandler = () => {
    if (!taskText || !taskStatus) {
      return;
    }
    onClick(taskText, taskStatus);
  };
  
  return (
    <div className={styles.container}>
      <InputGroup className="mb-3">
        <Form.Control
          aria-label="Text input"
          value={taskText}
          onChange={(e) => onChangeInput(e)}
          as="textarea" 
          rows={3}
        />
        <Form.Select
          aria-label="Default select example"
          value={taskStatus}
          onChange={(e) => onSelectStatus(e)}
        >
          <option value="new">new</option>
          <option value="in_progress">in progress</option>
          <option value="complited">complited</option>
          <option value="stuck">stuck</option>
        </Form.Select>
      </InputGroup>
      <Button variant="outline-primary" onClick={onClickHandler}>
        Send
      </Button>
    </div>
  );
};

export default AddTask;
