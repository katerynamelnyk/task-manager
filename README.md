This application created with Next.js, React calendar, React Bootstrap, PostgreSQL, Prisma.
You have a calendar, for each date you can add tasks.
You can create, edit, delete your tasks.
Each task has id, text, status and date.
Statuses: new, in progress, complited, stuck. 

All endpoints created in this app, so it is enough to clone the repo and use localserver. 